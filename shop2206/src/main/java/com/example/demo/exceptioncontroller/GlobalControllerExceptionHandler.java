package com.example.demo.exceptioncontroller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
class GlobalControllerExceptionHandler {
	@ResponseStatus(HttpStatus.GATEWAY_TIMEOUT)
	@ExceptionHandler(java.lang.ArithmeticException.class)
	public void handleArithmeticError() {
		System.out.println("Handle Arithemetic exception");
	}
}
