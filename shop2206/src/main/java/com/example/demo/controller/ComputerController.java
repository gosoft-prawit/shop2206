package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Computer;
import com.example.demo.repo.ComputerRepository;

@RestController
public class ComputerController {
	@Autowired
	ComputerRepository computerRepo;

	@GetMapping("/computers")
	@ResponseBody
	String getAllComputers() {
		Iterable<Computer> computers = computerRepo.findAll();
		String out = "";
		for (Computer computer : computers) {
			out = out + computer.getBrand();
		}
		return "Get Computer : " + out;
	}

	@PostMapping(path = "/computers", consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	String addComputer(@RequestBody Computer computer) {
		computerRepo.save(computer);

		return "Add Computer : " + computer.getBrand();
	}

	@PutMapping(path = "/computers/{brand}", consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	String updateComputer(@PathVariable("brand") String brand, @RequestBody Computer computer) {

		Iterable<Computer> computers = computerRepo.findAll();
		String out = "";
		for (Computer currentComputer : computers) {
			if (currentComputer.getBrand().equals(brand)) {
				currentComputer.setCpu(computer.getCpu());
				currentComputer.setRam(computer.getRam());
				currentComputer.setDisk(computer.getDisk());
				computerRepo.save(currentComputer);
				out = currentComputer.getBrand();
				break;
			}
		}
		return "Update Computer " + out;
	}

	@DeleteMapping("/computers/{brand}")
	@ResponseBody
	String deleteComputer(@PathVariable("brand") String brand) {

		Iterable<Computer> computers = computerRepo.findAll();
		String out = "";
		for (Computer currentComputer : computers) {
			if (currentComputer.getBrand().equals(brand)) {
				computerRepo.delete(currentComputer);
				out = currentComputer.getBrand();
				break;
			}
		}
		return "Delete Computer " + out;
	}

}
