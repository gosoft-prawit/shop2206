package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Manager;
import com.example.demo.repo.ManagerRepository;

@RestController
public class ManagerController {
	@Autowired
	ManagerRepository managerRepo;
	
	@GetMapping("/managers")
	@ResponseBody
	String getAllManagers() {
		Iterable<Manager> managers = managerRepo.findAll();
		String out = "";
		for (Manager manager : managers) {
			out = out + manager.getName();
		}
		return "Get Manager " + out;
	}

	@PostMapping(path = "/managers", consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	String addManager(@RequestBody Manager manager) {
		
		managerRepo.save(manager);
		return "Add Manager " + manager.getName();
	}

	@PutMapping(path = "/managers/{managerId}", consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	String updateManager(@PathVariable("managerId") int managerId, @RequestBody Manager manager) {
		Manager managerFromDB = managerRepo.findById(managerId).get();
		managerFromDB.setName(manager.getName());
		managerFromDB.setLastName(manager.getLastName());
		managerFromDB.setSalary(manager.getSalary());
		managerRepo.save(managerFromDB);

		return "Update Manager " + managerId;
	}

	@DeleteMapping("/managers/{managerId}")
	@ResponseBody
	String deleteUser(@PathVariable("managerId") int managerId) {
		Manager managerFromDB = managerRepo.findById(managerId).get();
		managerRepo.delete(managerFromDB);
		return "Delete Manager " + managerId;
	}
}