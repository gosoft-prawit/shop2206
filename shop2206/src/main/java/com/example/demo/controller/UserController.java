package com.example.demo.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

//import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.exception.InputFieldException;
import com.example.demo.model.Sale;
import com.example.demo.model.User;
import com.example.demo.repo.SaleRepository;
import com.example.demo.repo.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
public class UserController {
	private static final Logger logger = LoggerFactory.getLogger(InputFieldException.class);
	
	@Autowired UserRepository userRepo;
	@Autowired SaleRepository saleRepo;

	ArrayList<User> userList = new ArrayList<User>();

	@GetMapping("/test")
	@ResponseBody
	String test() {
		System.out.println(1 / 0);
		return "";
	}

	@GetMapping("/users")
	@ResponseBody
	String getAllUsers() {
		Iterable<User> users = userRepo.findAll();
		String out = "";
		for (User user : users) {
			out = out + user.getUsername();
		}
		return "Get User " + out;
	}
	

	@ResponseStatus(value=HttpStatus.CONFLICT, reason="Input field is wrong")
	@ExceptionHandler(InputFieldException.class)
	public void handleInputFieldException() {
		System.out.println("Handle Input Field Exception");
		logger.debug("This is Debug");
		logger.info("This is Info");
		logger.warn("This is Warn");
		logger.error("This is Error");
	}

	@PostMapping(path = "/users", consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	String addUser(@RequestBody User user) throws InputFieldException {
		
		if (user.getAge() > 100 || user.getAge() < 0) {
			throw new InputFieldException(false, false, true, false);
		}

		Set<Sale> sales = new HashSet<Sale>();

		Sale sale1 = new Sale();
		sale1.setName("Oak01");
		sale1.setSalary(10000);
		sales.add(sale1);

		Sale sale2 = new Sale();
		sale2.setName("Oak02");
		sale2.setSalary(20000);
		sales.add(sale2);

		user.setSales(sales);
		userRepo.save(user);
		sale1.setUser(user);
		sale2.setUser(user);
		saleRepo.save(sale1);
		saleRepo.save(sale2);

		return "Add User " + user.getUsername() + " password " + user.getPassword();
	}

	@PutMapping(path = "/users/{userId}", consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	String updateUser(@PathVariable("userId") int userId, @RequestBody User user) {
		User userFromDB = userRepo.findById(userId).get();
		userFromDB.setUserId(user.getUserId());
		userFromDB.setUsername(user.getUsername());
		userFromDB.setPassword(user.getPassword());
		userFromDB.setAge(user.getAge());
		userRepo.save(userFromDB);

		return "Update User " + userId;
	}

	@DeleteMapping("/users/{userId}")
	@ResponseBody
	String deleteUser(@PathVariable("userId") int userId) {
		User userFromDB = userRepo.findById(userId).get();
		userRepo.delete(userFromDB);
		return "Delete User " + userId;
	}
}
