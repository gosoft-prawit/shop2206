package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Staff;
import com.example.demo.repo.StaffRepository;

@RestController
public class StaffController {
	@Autowired
	StaffRepository staffRepo;
	
	@GetMapping("/staffs")
	@ResponseBody
	String getAllStaffs() {
		Iterable<Staff> staffs = staffRepo.findAll();
		String out = "";
		for (Staff staff : staffs) {
			out = out + staff.getName();
		}
		return "Get Staff " + out;
	}

	@PostMapping(path = "/staffs", consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	String addStaff(@RequestBody Staff staff) {
		
		staffRepo.save(staff);
		return "Add Staff " + staff.getName();
	}

	@PutMapping(path = "/staffs/{staffId}", consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	String updateStaff(@PathVariable("staffId") int staffId, @RequestBody Staff staff) {
		Staff staffFromDB = staffRepo.findById(staffId).get();
		staffFromDB.setName(staff.getName());
		staffFromDB.setLastName(staff.getLastName());
		staffFromDB.setSalary(staff.getSalary());
		staffRepo.save(staffFromDB);

		return "Update Staff " + staffId;
	}

	@DeleteMapping("/staffs/{staffId}")
	@ResponseBody
	String deleteUser(@PathVariable("staffId") int staffId) {
		Staff staffFromDB = staffRepo.findById(staffId).get();
		staffRepo.delete(staffFromDB);
		return "Delete Staff " + staffId;
	}
}