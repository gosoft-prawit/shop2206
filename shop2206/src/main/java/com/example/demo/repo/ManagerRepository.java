package com.example.demo.repo;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.model.Manager;

public interface ManagerRepository extends CrudRepository<Manager, Integer> {

}