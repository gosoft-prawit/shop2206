package com.example.demo.repo;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.model.Sale;

public interface SaleRepository extends CrudRepository<Sale, Integer> {

}