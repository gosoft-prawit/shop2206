package com.example.demo.repo;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.model.Computer;

public interface ComputerRepository extends CrudRepository<Computer, Integer> {

}