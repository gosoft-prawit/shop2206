package com.example.demo.repo;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.model.Staff;

public interface StaffRepository extends CrudRepository<Staff, Integer> {

}