package com.example.demo.exception;

public class InputFieldException extends Exception {
	private boolean isUserNameWrong;
	private boolean isPasswordWrong;
	private boolean isAgeWrong;
	private boolean isUserIdWrong;
	
	public InputFieldException(boolean isUserNameWrong, boolean isPasswordWrong, boolean isAgeWrong,
			boolean isUserIdWrong) {
		super();
		this.isUserNameWrong = isUserNameWrong;
		this.isPasswordWrong = isPasswordWrong;
		this.isAgeWrong = isAgeWrong;
		this.isUserIdWrong = isUserIdWrong;
		
	}
	
	public boolean isUserNameWrong() {
		return isUserNameWrong;
	}
	public void setUserNameWrong(boolean isUserNameWrong) {
		this.isUserNameWrong = isUserNameWrong;
	}
	public boolean isPasswordWrong() {
		return isPasswordWrong;
	}
	public void setPasswordWrong(boolean isPasswordWrong) {
		this.isPasswordWrong = isPasswordWrong;
	}
	public boolean isAgeWrong() {
		return isAgeWrong;
	}
	public void setAgeWrong(boolean isAgeWrong) {
		this.isAgeWrong = isAgeWrong;
	}
	public boolean isUserIdWrong() {
		return isUserIdWrong;
	}
	public void setUserIdWrong(boolean isUserIdWrong) {
		this.isUserIdWrong = isUserIdWrong;
	}
	
	
	
	

}
